from golang:1.8

RUN mkdir /go/src/app/
WORKDIR /go/src/app/

COPY . /go/src/app

RUN go get -d -v .
RUN go install -v .

CMD ["app"]
